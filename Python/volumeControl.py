#!/usr/bin/python

import sys
import os
import requests
import time

baseUrl = "http://192.168.1.50"
command = "/action"

code = 0
if(len(sys.argv) > 1):
    file = open(os.path.dirname(os.path.realpath(__file__)) +
                "/CodeMapping.conf", "r")
    for line in file.readlines():
        if line.split()[0].lower() == sys.argv[1].lower():
            code = line.split()[1]

if code != 0:
    session = requests.session()
    payload = {'code': code}
    sent = False
    while not sent:
        try:
            request = session.post(baseUrl + command, payload)
            sent = True
        except OSError:
            print("Network unreachable")
            time.sleep(0.001)

else:
    print("Error, command line argument not parsable or none given")
