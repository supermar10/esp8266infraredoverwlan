#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WiFiClient.h>
#include <IRremoteESP8266.h>
#include <IRsend.h>
#include <IRutils.h>
#include "Codes.h"
#include "Credentials.h"

// Pin D5 on NodeMCU
#define SEND_PIN 15
#define BAUD_RATE 115200
#define FREQUENCY 38

IRsend irsend(SEND_PIN);
ESP8266WebServer server(80);

void setup()
{
  Serial.begin(BAUD_RATE, SERIAL_8N1, SERIAL_TX_ONLY);

  WiFi.begin(ssid, password);
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.print("InfraredOverWlan is now running and is sending over Pin ");
  Serial.println(SEND_PIN);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP().toString());
  server.on("/", handleRoot);
  server.on("/action", handleIr);
  server.begin();
  irsend.begin();
}

void loop()
{
  server.handleClient();
}

void handleRoot()
{
  server.send(200, "text/html",
              "<html>"
              "<head><title>InfraredOverWlan</title></head>"
              "<body>"
              "<h1>Hello from ESP8266</h1>"
              "<form action=\"action\" method =\"post\">"
              "<button type=\"submit\" name=\"code\" value=\"1\">Quieter</button> "
              "<button type=\"submit\" name=\"code\" value=\"2\">Louder</button> "
              "<button type=\"submit\" name=\"code\" value=\"3\">Mute</button> "
              "<button type=\"submit\" name=\"code\" value=\"4\">Standby</button> "
              "</form>"
              "</body>"
              "</html>");
}

void handleIr()
{
  for (uint8_t i = 0; i < server.args(); i++)
  {
    if (server.argName(i) == "code")
    {
      uint32_t code = strtoul(server.arg(i).c_str(), NULL, 10);
      switch (code)
      {
      case 1:
        sendCode(quieter, sizeof(quieter));
        break;
      case 2:
        sendCode(louder, sizeof(louder));
        break;
      case 3:
        sendCode(mute, sizeof(mute));
        break;
      case 4:
        sendCode(standby, sizeof(standby));
        break;
      }
    }
  }
  handleRoot();
}

void sendCode(uint16_t code[], uint16_t size)
{
  irsend.sendRaw(code, size, FREQUENCY);
}